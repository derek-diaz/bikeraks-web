var mapOptions;
var map;

function initialize() {

    // Create an array of styles.
    var style = [
        {
            stylers: [
                {
                    hue: "#76c2af"
                },
                {
                    saturation: -20
                }
      ]
    }, {
            featureType: "road",
            elementType: "labels",
            stylers: [
                {
                    visibility: "off"
                }
      ]
    }
  ];

   // var styledMap = new google.maps.StyledMapType(styles, {
   //     name: "BikeRaks Style"
   // });



    var mapOptions = {
        center: {
            lat: 39.828127,
            lng: -98.579404
        },
        disableDefaultUI: true,
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        styles: style
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

   // map.mapTypes.set('map_style', styledMap);
  //  map.setMapTypeId('map_style');

    var bikeLayer = new google.maps.BicyclingLayer();
    bikeLayer.setMap(map);

    getLocation();
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {

    }
}

function showPosition(position) {
    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); //Makes a latlng
    map.panTo(latLng); //Make map global
    map.setZoom(15);
}


google.maps.event.addDomListener(window, 'load', initialize);

var markerCount;

$(document).ready(function () {

    //Initial poi request
    $.ajax({
        url: "index.php/BikeApi/raks",
        type: "GET",
        cache: false,
        crossDomain: true,
        dataType: "json",
        success: function (data) {
            var json = data;
            var l = json.length;
            if (l > 0) {
                for (var i = 0; i < l; i++) {
                    
                    //Creating Info Window for each POI
                    var html = "<b>" + json[i].feature_type + "</b><br/>";
                    html = html.concat("<b>Location Type: </b>" + json[i].location + "<br/>");
                    html = html.concat("<b>Parking Type: </b>" + json[i].parking_type + "<br/>");
                    html = html.concat("<b>Fees: </b>" + json[i].price + "<br/>");
                    html = html.concat("<b>Image:</b></br><img src=\"https://maps.googleapis.com/maps/api/streetview?size=400x200&location=" + json[i].y_cord + "," + json[i].x_cord + "&fov=90&heading=235&pitch=-10\"></img><br/>");
                    
                    
                    addMarkerToMap(json[i].y_cord, json[i].x_cord,html);
                }
            }
        },
        error: function (e) {
            console(e.responseText);
        }
    });
    
});

/*
* Add markers to map
*/
function addMarkerToMap(lat, long, htmlMarkupForInfoWindow){
    var infowindow = new google.maps.InfoWindow();
    var myLatLng = new google.maps.LatLng(lat, long);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        animation: google.maps.Animation.DROP,
    });
     
    //Gives each marker an Id for the on click
    markerCount++;
 
    //Creates the event listener for clicking the marker
    //and places the marker on the map
    google.maps.event.addListener(marker, 'click', (function(marker, markerCount) {
        return function() {
            infowindow.setContent(htmlMarkupForInfoWindow);
            infowindow.open(map, marker);
        }
    })(marker, markerCount)); 
     
    //Pans map to the new location of the marker
    map.panTo(myLatLng)       
}



