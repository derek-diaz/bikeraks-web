<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BikeApi extends CI_Controller {

	public function index()
	{
		echo "API!";
	}
    
    
    public function raks(){
        $this->load->model('bikerak_model');
        $this->output->set_content_type('application/json');
        $raks = $this->bikerak_model->get_raks();
        
        echo json_encode($raks);
    }
}

?>