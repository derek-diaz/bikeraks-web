<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
    public $limit = 10;
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library(array('table','form_validation'));
        $this->load->helper('url');
    }

    public function index()
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['lname'] = $session_data['lname'];
            $data['fname'] = $session_data['fname'];

            $data['users'] = $this->user_model->get_all_users();
            $this->load->view('user/index',$data);


            /*$uri_segment = 3;
            $offset = $this->uri->segment($uri_segment);

            $users = $this->user_model->get_paged_list($this->limit, $offset)->result();

            // generate pagination
            $this->load->library('pagination');
            $config['base_url'] = site_url('user');
            $config['total_rows'] = $this->user_model->count_all();
            $config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();

            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('ID', 'First Name', 'Last Name', 'Username', 'Email', 'Actions');
            $i = 0 + $offset;
            foreach ($users as $user){
                $this->table->add_row(++$i, $user->fname, $user->lname, $user->username,$user->email,
                    anchor('user/update/'.$user->id,'update',array('class'=>'btn btn-success')).' '.
                    anchor('user/delete/'.$user->id,'delete',array('class'=>'btn btn-danger','onclick'=>"return confirm('Are you sure want to delete this person?')"))
                );
            }
            $tmpl = array (
                'table_open'          => '<table class="table table-striped table-bordered table-hover">',

                'heading_row_start'   => '<tr>',
                'heading_row_end'     => '</tr>',
                'heading_cell_start'  => '<th>',
                'heading_cell_end'    => '</th>',

                'row_start'           => '<tr>',
                'row_end'             => '</tr>',
                'cell_start'          => '<td>',
                'cell_end'            => '</td>',

                'row_alt_start'       => '<tr>',
                'row_alt_end'         => '</tr>',
                'cell_alt_start'      => '<td>',
                'cell_alt_end'        => '</td>',

                'table_close'         => '</table>'
            );

            $this->table->set_template($tmpl);

            $data['table'] = $this->table->generate();

            $this->load->view('users_view', $data);*/
        }else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    /*
     * Adding new users
     */
    function add()
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['lname'] = $session_data['lname'];
            $data['fname'] = $session_data['fname'];
            $this->load->library('form_validation');

            $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
            $this->form_validation->set_rules('username', 'Username', 'required|max_length[255]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[255]');
            $this->form_validation->set_rules('fname', 'Fname', 'required|max_length[255]');
            $this->form_validation->set_rules('lname', 'Lname', 'required|max_length[255]');
            $this->form_validation->set_rules('role', 'Role', 'required|max_length[255]');

            if ($this->form_validation->run()) {
                $params = array(
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                    'password' => $this->input->post('password'),
                    'fname' => $this->input->post('fname'),
                    'lname' => $this->input->post('lname'),
                    'role' => $this->input->post('role'),
                    'creation_date' => $this->input->post('creation_date'),
                );

                $users_id = $this->user_model->add_users($params);
                redirect('user/index');
            } else {
                $this->load->view('user/add');
            }
        }else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    /*
     * Editing users
     */
    function edit($id)
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['lname'] = $session_data['lname'];
            $data['fname'] = $session_data['fname'];
            $this->load->library('form_validation');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
            $this->form_validation->set_rules('username', 'Username', 'required|max_length[255]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[255]');
            $this->form_validation->set_rules('fname', 'Fname', 'required|max_length[255]');
            $this->form_validation->set_rules('lname', 'Lname', 'required|max_length[255]');
            $this->form_validation->set_rules('role', 'Role', 'required|max_length[255]');

            if ($this->form_validation->run()) {
                $params = array(
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                    'password' => $this->input->post('password'),
                    'fname' => $this->input->post('fname'),
                    'lname' => $this->input->post('lname'),
                    'role' => $this->input->post('role'),
                    'creation_date' => $this->input->post('creation_date'),
                );

                $this->user_model->update_users($id, $params);
                redirect('user/index');
            } else {
                $data['users'] = $this->user_model->get_users($id);
                $this->load->view('user/edit', $data);
            }
        }else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    /*
     * Deleting users
     */
    function remove($id)
    {
        $this->User_model->delete_users($id);
        redirect('user/index');
    }
}
