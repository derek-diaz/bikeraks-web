<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index()
    {
        if($this->session->userdata('logged_in')){
            redirect('dashboard', 'refresh');
        }else{
        $this->load->helper(array('form'));
        $this->load->view('login');
            }
    }

}
