<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bikerak extends CI_Controller
{
    public $limit = 10;

    function __construct()
    {
        parent::__construct();
        $this->load->model('bikerak_model');
        $this->load->library(array('table','form_validation'));
        $this->load->helper('url');
    }

    public function index($offset = 0)
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['lname'] = $session_data['lname'];
            $data['fname'] = $session_data['fname'];

            $uri_segment = 3;
            $offset = $this->uri->segment($uri_segment);

            $bikeraks = $this->bikerak_model->get_paged_list($this->limit, $offset)->result();

            // generate pagination
            $this->load->library('pagination');
            $config['base_url'] = site_url('bikerak');
            $config['total_rows'] = $this->bikerak_model->count_all();
            $config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();

            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('ID', 'Parking Type', 'Feature Type', 'X Coordinate', 'Y Coordinate','Location','Address','State','City','Bike Spaces','Access','Price','Actions');
            $i = 0 + $offset;
            foreach ($bikeraks as $bikerak){
                $this->table->add_row(++$i, $bikerak->parking_type,$bikerak->feature_type,$bikerak->x_cord,$bikerak->y_cord,$bikerak->location,$bikerak->address,$bikerak->state,$bikerak->city,$bikerak->bike_spaces,$bikerak->access,$bikerak->price,
                    anchor('bikerak/update/'.$bikerak->parking_id,'update',array('class'=>'btn btn-success')).' '.
                    anchor('bikerak/delete/'.$bikerak->parking_id,'delete',array('class'=>'btn btn-danger','onclick'=>"return confirm('Are you sure want to delete this person?')"))
                );
            }
            $tmpl = array (
                'table_open'          => '<table class="table table-striped table-bordered table-hover">',

                'heading_row_start'   => '<tr>',
                'heading_row_end'     => '</tr>',
                'heading_cell_start'  => '<th>',
                'heading_cell_end'    => '</th>',

                'row_start'           => '<tr>',
                'row_end'             => '</tr>',
                'cell_start'          => '<td>',
                'cell_end'            => '</td>',

                'row_alt_start'       => '<tr>',
                'row_alt_end'         => '</tr>',
                'cell_alt_start'      => '<td>',
                'cell_alt_end'        => '</td>',

                'table_close'         => '</table>'
            );

            $this->table->set_template($tmpl);

            $data['table'] = $this->table->generate();

            //$this->load->view('bikeraks_view', $data);
            $data['bikerak'] = $this->bikerak_model->get_all_bikerak();
            $this->load->view('bikerak/index',$data);
        }else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    /*
    * Adding new bikerak
    */
    function add()
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['lname'] = $session_data['lname'];
            $data['fname'] = $session_data['fname'];
            $this->load->library('form_validation');

            $this->form_validation->set_rules('parking_type', 'Parking Type', 'required|max_length[255]');
            $this->form_validation->set_rules('parking_type2', 'Parking Type2', 'required|max_length[255]');
            $this->form_validation->set_rules('parking_type3', 'Parking Type3', 'required|max_length[255]');
            $this->form_validation->set_rules('feature_type', 'Feature Type', 'required|max_length[255]');
            $this->form_validation->set_rules('x_cord', 'X Cord', 'required');
            $this->form_validation->set_rules('y_cord', 'Y Cord', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required|max_length[255]');
            $this->form_validation->set_rules('address', 'Address', 'required|max_length[255]');
            $this->form_validation->set_rules('state', 'State', 'required|max_length[255]');
            $this->form_validation->set_rules('city', 'City', 'required|max_length[255]');
            $this->form_validation->set_rules('bike_spaces', 'Bike Spaces', 'required|integer');
            $this->form_validation->set_rules('access', 'Access', 'required|max_length[255]');
            $this->form_validation->set_rules('price', 'Price', 'required|max_length[255]');

            if ($this->form_validation->run()) {
                $params = array(
                    'parking_type' => $this->input->post('parking_type'),
                    'parking_type2' => $this->input->post('parking_type2'),
                    'parking_type3' => $this->input->post('parking_type3'),
                    'feature_type' => $this->input->post('feature_type'),
                    'x_cord' => $this->input->post('x_cord'),
                    'y_cord' => $this->input->post('y_cord'),
                    'location' => $this->input->post('location'),
                    'address' => $this->input->post('address'),
                    'state' => $this->input->post('state'),
                    'city' => $this->input->post('city'),
                    'bike_spaces' => $this->input->post('bike_spaces'),
                    'access' => $this->input->post('access'),
                    'price' => $this->input->post('price'),
                    'create_dttm' => $this->input->post('create_dttm'),
                    'change_dttm' => $this->input->post('change_dttm'),
                );

                $bikerak_id = $this->Bikerak_model->add_bikerak($params);
                redirect('bikerak/index');
            } else {
                $this->load->view('bikerak/add', $data);
            }
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    /*
     * Editing bikerak
     */
    function edit($parking_id)
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['lname'] = $session_data['lname'];
            $data['fname'] = $session_data['fname'];
            $this->load->library('form_validation');


            $this->form_validation->set_rules('parking_type', 'Parking Type', 'required|max_length[255]');
            $this->form_validation->set_rules('parking_type2', 'Parking Type2', 'required|max_length[255]');
            $this->form_validation->set_rules('parking_type3', 'Parking Type3', 'required|max_length[255]');
            $this->form_validation->set_rules('feature_type', 'Feature Type', 'required|max_length[255]');
            $this->form_validation->set_rules('x_cord', 'X Cord', 'required');
            $this->form_validation->set_rules('y_cord', 'Y Cord', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required|max_length[255]');
            $this->form_validation->set_rules('address', 'Address', 'required|max_length[255]');
            $this->form_validation->set_rules('state', 'State', 'required|max_length[255]');
            $this->form_validation->set_rules('city', 'City', 'required|max_length[255]');
            $this->form_validation->set_rules('bike_spaces', 'Bike Spaces', 'required|integer');
            $this->form_validation->set_rules('access', 'Access', 'required|max_length[255]');
            $this->form_validation->set_rules('price', 'Price', 'required|max_length[255]');

            if ($this->form_validation->run()) {
                $params = array(
                    'parking_type' => $this->input->post('parking_type'),
                    'parking_type2' => $this->input->post('parking_type2'),
                    'parking_type3' => $this->input->post('parking_type3'),
                    'feature_type' => $this->input->post('feature_type'),
                    'x_cord' => $this->input->post('x_cord'),
                    'y_cord' => $this->input->post('y_cord'),
                    'location' => $this->input->post('location'),
                    'address' => $this->input->post('address'),
                    'state' => $this->input->post('state'),
                    'city' => $this->input->post('city'),
                    'bike_spaces' => $this->input->post('bike_spaces'),
                    'access' => $this->input->post('access'),
                    'price' => $this->input->post('price'),
                    'create_dttm' => $this->input->post('create_dttm'),
                    'change_dttm' => $this->input->post('change_dttm'),
                );

                $this->Bikerak_model->update_bikerak($parking_id, $params);
                redirect('bikerak/index');
            } else {
                $data['bikerak'] = $this->bikerak_model->get_bikerak($parking_id);
                $this->load->view('bikerak/edit', $data);
            }
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    /*
     * Deleting bikerak
     */
    function remove($parking_id)
    {
        $this->bikerak_model->delete_bikerak($parking_id);
        redirect('bikerak/index');
    }
}
?>