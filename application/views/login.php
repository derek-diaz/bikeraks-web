<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BIKERAKS - Login</title>

    <!-- Bootstrap -->
    <link href="public/css/bootstrap.css" rel="stylesheet">
    <link href="public/css/style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <LINK REL="SHORTCUT ICON" HREF="public/img/favicon.ico">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <a href="../" class="navbar-brand"><img src="public/img/logo.png" alt="BIKERAKS" class="logo"/><span class="logoText">BIKERAKS</span></a>

            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav">
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/login" class="font"></a></li>
            </ul>

        </div>
    </div>
</div>
<div class="row container-center box-top">
<div class="text-center pagination-centered center-box">

    <div class="panel panel-default .col-md-6">
        <div class="panel-heading">
            <h3 class="panel-title">BIKERAKS Login</h3>
        </div>
        <div class="panel-body">

            <?php echo validation_errors(); ?>
            <?php echo form_open('verifylogin'); ?>
            <div class="input-group input-group-md">
                <span class="input-group-addon" id="sizing-addon1">Username</span>
                <input type="text" id="username" name="username" class="form-control" aria-describedby="sizing-addon1">
            </div><br/>
            <div class="input-group input-group-md">
                <span class="input-group-addon" id="sizing-addon1">Password</span>
                <input type="password" id="passowrd" name="password" class="form-control" aria-describedby="sizing-addon1">
            </div><br/>
            <input class="btn btn-default" type="submit" value="Login"/>
            </form>
        </div>
    </div>

</div>
</div>







<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="public/js/bootstrap.min.js"></script>
</body>
</html>