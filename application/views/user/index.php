<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>BIKERAKS - ADMIN</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="<?php echo base_url('/public/css/admin/bootstrap.css')?>" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?php echo base_url('/public/css/admin/font-awesome.css')?>" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="<?php echo base_url('/public/css/admin/basic.css')?>" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="<?php echo base_url('/public/css/admin/custom.css')?>" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <LINK REL="SHORTCUT ICON" HREF="<?php echo base_url('public/img/favicon.ico')?>">
</head>

<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/dashboard">BIKERAKS</a>
        </div>

        <div class="header-right">
            <a href="dashboard/logout" class="btn btn-danger" title="Logout">LOGOUT</a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <div class="user-img-div">
                        <img src="public/img/profile-placeholder.png" class="img-thumbnail"/>

                        <div class="inner-text">
                            <?php echo $fname ?> <?php echo $lname ?>
                            <br/>
                            <small><?php echo $username ?></small>
                        </div>
                    </div>

                </li>


                <li>
                    <a href="/dashboard"><i class="fa fa-dashboard "></i>Dashboard</a>
                </li>

                <li>
                    <a href="/bikerak"><i class="fa fa-bicycle "></i>BikeRaks</a>
                </li>
                <li>
                    <a class="active-menu" href="/user"><i class="fa fa-anchor "></i>Users</a>
                </li>
            </ul>
        </div>

    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">BIKERAKS</h1>

                    <h1 class="page-subhead-line">In this page you can see and add new Bikeraks.</h1>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Bikerak List
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">

                                <div class="pull-right">
                                    <a href="<?php echo site_url('user/add'); ?>" class="btn btn-success">Add</a>
                                </div>

                                <table  class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <td>ID</td>
                                        <td>Email</td>
                                        <td>Username</td>
                                        <td>Password</td>
                                        <td>Fname</td>
                                        <td>Lname</td>
                                        <td>Role</td>
                                        <td>Creation Date</td>
                                        <td>Actions</td>
                                    </tr>
                                    <?php foreach($users as $u): ?>
                                        <tr>
                                            <td><?php echo $u['id']; ?></td>
                                            <td><?php echo $u['email']; ?></td>
                                            <td><?php echo $u['username']; ?></td>
                                            <td><?php echo $u['password']; ?></td>
                                            <td><?php echo $u['fname']; ?></td>
                                            <td><?php echo $u['lname']; ?></td>
                                            <td><?php echo $u['role']; ?></td>
                                            <td><?php echo $u['creation_date']; ?></td>
                                            <td>
                                                <a href="<?php echo site_url('user/edit/'.$u['id']); ?>" class="btn btn-info">Edit</a>
                                                <a href="<?php echo site_url('user/remove/'.$u['id']); ?>" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            SOME CONTENT
                        </div>
                    </div>
                </div>

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <div id="footer-sec">
        &copy; 2016 BIKERAKS
    </div>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="<?php echo base_url('/public/js/admin/jquery-1.10.2.js')?>"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="<?php echo base_url('/public/js/admin/bootstrap.js')?>"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="<?php echo base_url('/public/js/admin/jquery.metisMenu.js')?>"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="<?php echo base_url('/public/js/admin/custom.js')?>"></script>


</body>
</html>