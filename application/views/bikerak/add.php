<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>BIKERAKS - ADMIN</title>

	<!-- BOOTSTRAP STYLES-->
	<link href="<?php echo base_url('/public/css/admin/bootstrap.css')?>" rel="stylesheet" />
	<!-- FONTAWESOME STYLES-->
	<link href="<?php echo base_url('/public/css/admin/font-awesome.css')?>" rel="stylesheet" />
	<!--CUSTOM BASIC STYLES-->
	<link href="<?php echo base_url('/public/css/admin/basic.css')?>" rel="stylesheet" />
	<!--CUSTOM MAIN STYLES-->
	<link href="<?php echo base_url('/public/css/admin/custom.css')?>" rel="stylesheet" />
	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
	<LINK REL="SHORTCUT ICON" HREF="<?php echo base_url('public/img/favicon.ico')?>">
</head>

<body>
<div id="wrapper">
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/dashboard">BIKERAKS</a>
		</div>

		<div class="header-right">
			<a href="dashboard/logout" class="btn btn-danger" title="Logout">LOGOUT</a>
		</div>
	</nav>
	<!-- /. NAV TOP  -->
	<nav class="navbar-default navbar-side" role="navigation">
		<div class="sidebar-collapse">
			<ul class="nav" id="main-menu">
				<li>
					<div class="user-img-div">
						<img src="../public/img/profile-placeholder.png" class="img-thumbnail" />

						<div class="inner-text">
							<?php echo $fname ?> <?php echo $lname ?>
							<br />
							<small><?php echo $username ?></small>
						</div>
					</div>

				</li>


				<li>
					<a href="/dashboard"><i class="fa fa-dashboard "></i>Dashboard</a>
				</li>

				<li>
					<a class="active-menu" href="/bikerak"><i class="fa fa-bicycle "></i>BikeRaks</a>
				</li>
				<li>
					<a href="/user"><i class="fa fa-anchor "></i>Users</a>
				</li>
			</ul>
		</div>

	</nav>
	<!-- /. NAV SIDE  -->
	<div id="page-wrapper">
		<div id="page-inner">
			<div class="row">
				<div class="col-md-12">
					<h1 class="page-head-line">BIKERAKS</h1>
					<h1 class="page-subhead-line">In this page you can see and add new Bikeraks.</h1>

					<div class="panel panel-default">
						<div class="panel-heading">
							Add Bikerak
						</div>
						<div class="panel-body">
							<?php echo validation_errors(); ?>

							<?php echo form_open('bikerak/add',array("class"=>"form-horizontal")); ?>

							<div class="form-group">
								<label for="parking_type" class="col-md-4 control-label">Parking Type</label>
								<div class="col-md-8">
									<input type="text" name="parking_type" value="<?php echo $this->input->post('parking_type'); ?>" class="form-control" id="parking_type" />
								</div>
							</div>
							<div class="form-group">
								<label for="parking_type2" class="col-md-4 control-label">Parking Type2</label>
								<div class="col-md-8">
									<input type="text" name="parking_type2" value="<?php echo $this->input->post('parking_type2'); ?>" class="form-control" id="parking_type2" />
								</div>
							</div>
							<div class="form-group">
								<label for="parking_type3" class="col-md-4 control-label">Parking Type3</label>
								<div class="col-md-8">
									<input type="text" name="parking_type3" value="<?php echo $this->input->post('parking_type3'); ?>" class="form-control" id="parking_type3" />
								</div>
							</div>
							<div class="form-group">
								<label for="feature_type" class="col-md-4 control-label">Feature Type</label>
								<div class="col-md-8">
									<input type="text" name="feature_type" value="<?php echo $this->input->post('feature_type'); ?>" class="form-control" id="feature_type" />
								</div>
							</div>
							<div class="form-group">
								<label for="x_cord" class="col-md-4 control-label">X Cord</label>
								<div class="col-md-8">
									<input type="text" name="x_cord" value="<?php echo $this->input->post('x_cord'); ?>" class="form-control" id="x_cord" />
								</div>
							</div>
							<div class="form-group">
								<label for="y_cord" class="col-md-4 control-label">Y Cord</label>
								<div class="col-md-8">
									<input type="text" name="y_cord" value="<?php echo $this->input->post('y_cord'); ?>" class="form-control" id="y_cord" />
								</div>
							</div>
							<div class="form-group">
								<label for="location" class="col-md-4 control-label">Location</label>
								<div class="col-md-8">
									<input type="text" name="location" value="<?php echo $this->input->post('location'); ?>" class="form-control" id="location" />
								</div>
							</div>
							<div class="form-group">
								<label for="address" class="col-md-4 control-label">Address</label>
								<div class="col-md-8">
									<input type="text" name="address" value="<?php echo $this->input->post('address'); ?>" class="form-control" id="address" />
								</div>
							</div>
							<div class="form-group">
								<label for="state" class="col-md-4 control-label">State</label>
								<div class="col-md-8">
									<input type="text" name="state" value="<?php echo $this->input->post('state'); ?>" class="form-control" id="state" />
								</div>
							</div>
							<div class="form-group">
								<label for="city" class="col-md-4 control-label">City</label>
								<div class="col-md-8">
									<input type="text" name="city" value="<?php echo $this->input->post('city'); ?>" class="form-control" id="city" />
								</div>
							</div>
							<div class="form-group">
								<label for="bike_spaces" class="col-md-4 control-label">Bike Spaces</label>
								<div class="col-md-8">
									<input type="text" name="bike_spaces" value="<?php echo $this->input->post('bike_spaces'); ?>" class="form-control" id="bike_spaces" />
								</div>
							</div>
							<div class="form-group">
								<label for="access" class="col-md-4 control-label">Access</label>
								<div class="col-md-8">
									<input type="text" name="access" value="<?php echo $this->input->post('access'); ?>" class="form-control" id="access" />
								</div>
							</div>
							<div class="form-group">
								<label for="price" class="col-md-4 control-label">Price</label>
								<div class="col-md-8">
									<input type="text" name="price" value="<?php echo $this->input->post('price'); ?>" class="form-control" id="price" />
								</div>
							</div>
							<div class="form-group">
								<label for="create_dttm" class="col-md-4 control-label">Create Dttm</label>
								<div class="col-md-8">
									<input type="text" name="create_dttm" value="<?php echo $this->input->post('create_dttm'); ?>" class="form-control" id="create_dttm" />
								</div>
							</div>
							<div class="form-group">
								<label for="change_dttm" class="col-md-4 control-label">Change Dttm</label>
								<div class="col-md-8">
									<input type="text" name="change_dttm" value="<?php echo $this->input->post('change_dttm'); ?>" class="form-control" id="change_dttm" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-8">
									<button type="submit" class="btn btn-success">Save</button>
								</div>
							</div>

							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
			<!-- /. ROW  -->
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info">
						SOME CONTENT
					</div>
				</div>
			</div>

		</div>
		<!-- /. PAGE INNER  -->
	</div>
	<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<div id="footer-sec">
	&copy; 2016 BIKERAKS
</div>
<!-- /. FOOTER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="<?php echo base_url('/public/js/admin/jquery-1.10.2.js')?>"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="<?php echo base_url('/public/js/admin/bootstrap.js')?>"></script>
<!-- METISMENU SCRIPTS -->
<script src="<?php echo base_url('/public/js/admin/jquery.metisMenu.js')?>"></script>
<!-- CUSTOM SCRIPTS -->
<script src="<?php echo base_url('/public/js/admin/custom.js')?>"></script>


</body>
</html>