<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>BIKERAKS - ADMIN</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="<?php echo base_url('/public/css/admin/bootstrap.css')?>" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?php echo base_url('/public/css/admin/font-awesome.css')?>" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="<?php echo base_url('/public/css/admin/basic.css')?>" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="<?php echo base_url('/public/css/admin/custom.css')?>" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <LINK REL="SHORTCUT ICON" HREF="<?php echo base_url('public/img/favicon.ico')?>">
</head>

<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/dashboard">BIKERAKS</a>
        </div>

        <div class="header-right">
            <a href="dashboard/logout" class="btn btn-danger" title="Logout">LOGOUT</a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <div class="user-img-div">
                        <img src="public/img/profile-placeholder.png" class="img-thumbnail"/>

                        <div class="inner-text">
                            <?php echo $fname ?> <?php echo $lname ?>
                            <br/>
                            <small><?php echo $username ?></small>
                        </div>
                    </div>

                </li>


                <li>
                    <a href="/dashboard"><i class="fa fa-dashboard "></i>Dashboard</a>
                </li>

                <li>
                    <a class="active-menu" href="/bikerak"><i class="fa fa-bicycle "></i>BikeRaks</a>
                </li>
                <li>
                    <a href="/user"><i class="fa fa-anchor "></i>Users</a>
                </li>
            </ul>
        </div>

    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">BIKERAKS</h1>

                    <h1 class="page-subhead-line">In this page you can see and add new Bikeraks.</h1>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Bikerak List
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">

                                <div class="pull-right">
                                    <a href="<?php echo site_url('bikerak/add'); ?>" class="btn btn-success">Add</a>
                                </div>

                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <td>Parking Id</td>
                                        <td>Parking Type</td>
                                        <td>Parking Type2</td>
                                        <td>Parking Type3</td>
                                        <td>Feature Type</td>
                                        <td>X Cord</td>
                                        <td>Y Cord</td>
                                        <td>Location</td>
                                        <td>Address</td>
                                        <td>State</td>
                                        <td>City</td>
                                        <td>Bike Spaces</td>
                                        <td>Access</td>
                                        <td>Price</td>
                                        <td>Create Dttm</td>
                                        <td>Change Dttm</td>
                                        <td>Actions</td>
                                    </tr>
                                    <?php foreach ($bikerak as $b): ?>
                                        <tr>
                                            <td><?php echo $b['parking_id']; ?></td>
                                            <td><?php echo $b['parking_type']; ?></td>
                                            <td><?php echo $b['parking_type2']; ?></td>
                                            <td><?php echo $b['parking_type3']; ?></td>
                                            <td><?php echo $b['feature_type']; ?></td>
                                            <td><?php echo $b['x_cord']; ?></td>
                                            <td><?php echo $b['y_cord']; ?></td>
                                            <td><?php echo $b['location']; ?></td>
                                            <td><?php echo $b['address']; ?></td>
                                            <td><?php echo $b['state']; ?></td>
                                            <td><?php echo $b['city']; ?></td>
                                            <td><?php echo $b['bike_spaces']; ?></td>
                                            <td><?php echo $b['access']; ?></td>
                                            <td><?php echo $b['price']; ?></td>
                                            <td><?php echo $b['create_dttm']; ?></td>
                                            <td><?php echo $b['change_dttm']; ?></td>
                                            <td>
                                                <a href="<?php echo site_url('bikerak/edit/' . $b['parking_id']); ?>"
                                                   class="btn btn-info">Edit</a>
                                                <a href="<?php echo site_url('bikerak/remove/' . $b['parking_id']); ?>"
                                                   class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            SOME CONTENT
                        </div>
                    </div>
                </div>

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <div id="footer-sec">
        &copy; 2016 BIKERAKS
    </div>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="<?php echo base_url('/public/js/admin/jquery-1.10.2.js')?>"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="<?php echo base_url('/public/js/admin/bootstrap.js')?>"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="<?php echo base_url('/public/js/admin/jquery.metisMenu.js')?>"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="<?php echo base_url('/public/js/admin/custom.js')?>"></script>


</body>
</html>