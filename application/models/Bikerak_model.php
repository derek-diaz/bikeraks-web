<?php
class Bikerak_model extends CI_Model {

    // table name
    private $tbl= 'bikerak';

        public function __construct()
        {
               $this->load->database();
        }
    
    
    public function get_raks ()
{
   
        $query = $this->db->get('bikerak');
        return $query->result();
        //return $query->row_array();
}
    function count_all(){
        return $this->db->count_all($this->tbl);
    }
    // get persons with paging
    function get_paged_list($limit = 10, $offset = 0){
        $this->db->order_by('parking_id','asc');
        return $this->db->get($this->tbl, $limit, $offset);
    }

    /*
     * get bikerak by parking_id
     */
    function get_bikerak($parking_id)
    {
        return $this->db->get_where('bikerak',array('parking_id'=>$parking_id))->row_array();
    }

    /*
     * get all bikerak
     */
    function get_all_bikerak()
    {
        return $this->db->get('bikerak')->result_array();
    }

    /*
     * function to add bikerak
     */
    function add_bikerak($params)
    {
        $this->db->insert('bikerak',$params);
        return $this->db->insert_id();
    }

    /*
     * function to update bikerak
     */
    function update_bikerak($parking_id,$params)
    {
        $this->db->where('parking_id',$id);
        $this->db->update('bikerak',$params);
    }

    /*
     * function to delete bikerak
     */
    function delete_bikerak($parking_id)
    {
        $this->db->delete('bikerak',array('parking_id'=>$parking_id));
    }
}
?>