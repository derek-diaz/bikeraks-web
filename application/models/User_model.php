<?php
Class User_model extends CI_Model
{
    // table name
    private $tbl_users= 'users';

    public function __construct()
    {
        $this->load->database();
    }

    function login($username, $password)
    {
        $this -> db -> select('id, username, password, fname, lname, email');
        $this -> db -> from('users');
        $this -> db -> where('username', $username);
        $this -> db -> where('password', MD5($password));
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    /*
 * get users by id
 */
    function get_users($id)
    {
        return $this->db->get_where('users',array('id'=>$id))->row_array();
    }

    /*
     * get all users
     */
    function get_all_users()
    {
        return $this->db->get('users')->result_array();
    }

    /*
     * function to add users
     */
    function add_users($params)
    {
        $this->db->insert('users',$params);
        return $this->db->insert_id();
    }

    /*
     * function to update users
     */
    function update_users($id,$params)
    {
        $this->db->where('id',$id);
        $this->db->update('users',$params);
    }

    /*
     * function to delete users
     */
    function delete_users($id)
    {
        $this->db->delete('users',array('id'=>$id));
    }
}
?>